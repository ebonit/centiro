# PHP api-wrapper for the Centiro SOAP webservice

![Latest Stable Version](https://packagist.org/packages/ebonit/centiro) ![Total Downloads](https://packagist.org/packages/ebonit/centiro) ![License](https://packagist.org/packages/ebonit/centiro)

## Installation:

```shell
composer require ebonit/centiro
```

## Basic Usage:

```shell
use \Ebonit\Centiro\Client;

$client = new Client($cntiro_username, $centiro_password);
```

Example to create a new shipment:
shipment requirements can be found at 

```
Ebonit\Centiro\Method\Shipment
```

```
$shipment['Addresses'] = [$this->createAddressData($arguments)]; 
$shipment['Attributes'] = $this->createAttributes($pd, $format);
$shipment['Parcels'] = [$this->createParcelData($arguments)];            
$shipment['OrderNumber'] = $shipmentIdentifier;
$shipment['SenderCode'] = $_ASENDIA_CUSTOMER_ID);
$shipment['ShipDate'] = date("Y-m-d", strtotime('+2 days')).'T'.date("H:i:s");
$shipment['ShipmentIdentifier'] = $shipmentIdentifier;
$shipment['Currency'] = 'EUR';
```


Example to create parcel and print the sticker:
parcel, details can be found at
 
```
Ebonit\Centiro\Method\Parcel
```

```
$parcelDetails = [...aarray with required details...];
$response = $client->addAndPrintShipment($shipment);
```

To save the sticker data (zpl, pdf is also possible):

```
$printFile = base_path() . "/storage/centiro_zpl/" . $response->Shipments->SequenceNumber . ".zpl";
file_put_contents($printFile, base64_decode($response->ParcelDocuments->ParcelDocument->Content));
```