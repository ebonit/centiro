<?php
/*
 *  Centiro shipment API Client for Asendia
 * 
 *  (c) Eric Bontenbal web development <info@ericbontenbal.nl>
 */

namespace Ebonit\Centiro;

use \Ebonit\Centiro\Method\Shipment;
use \Ebonit\Centiro\Method\MasterShipment;
use \Ebonit\Centiro\Soap\SoapClient;

class CentiroWsdlClient implements CentiroWsdlClientInterface 
{
    private $authenticate;
    private $AuthenticationTicket;
    private $MessageId;
    private $client;
    private $test;
    private $wsdl_auth = 'https://uat.centiro.com/Universe.Services/TMSBasic/Wcf/c1/i1/TMSBasic/Authenticate.svc?wsdl';
    private $wsdl_basic = 'https://uat.centiro.com/Universe.Services/TMSBasic/Wcf/c1/i1/TMSBasic/TMSBasic.svc?wsdl';
    private $location_auth = 'https://cloud.centiro.com/Universe.Services/TMSBasic/Wcf/c1/i1/TMSBasic/Authenticate.svc/xml';
    private $location_basic = 'https://cloud.centiro.com/Universe.Services/TMSBasic/Wcf/c1/i1/TMSBasic/TMSBasic.svc/xml';
    private $location_auth_test = 'https://uat.centiro.com/Universe.Services/TMSBasic/Wcf/c1/i1/TMSBasic/Authenticate.svc/xml';
    private $location_basic_test = 'https://uat.centiro.com/Universe.Services/TMSBasic/Wcf/c1/i1/TMSBasic/TMSBasic.svc/xml';
    
    public function __construct($userName, $password, $test = false){

        $this->MessageId = null;
        $this->test = $test;
        $this->authenticate($userName, $password);
        $this->client = new SoapClient($this->wsdl_basic,['trace' => 1, 'soap_version' => SOAP_1_1]);
        if($test){
            $this->client->__setLocation($this->location_basic_test);
        }else{
            $this->client->__setLocation($this->location_basic);
        }
        $this->client->__setSoapHeaders($this->inputHeaders());
    }
    
    public function authenticate($userName, $password){
        $client = new \SoapClient($this->wsdl_auth,['trace' => 1, 'soap_version' => SOAP_1_1]);
        if($this->test){            
            $client->__setLocation($this->location_auth_test);
        }else{
            $client->__setLocation($this->location_auth);
        }
        $authenticate = $client->Authenticate(['UserName' => $userName, 'Password' => $password]);
        if(!$authenticate->AuthenticationTicket){
            throw new \Exception('Centiro Authentication error: ' . __LINE__ . '-' . $client->__getLastResponse() . ' - ' . $client->__getLastRequest());
        }
        $this->AuthenticationTicket = $authenticate->AuthenticationTicket;
    }
    
    private function inputHeaders(){
        $obj = new \stdClass();
        $messageId = 'dat:MessageId';//dat: is important 
        $authticket = 'dat:AuthenticationTicket';//dat: is important
        $obj->$messageId = $this->MessageId;
        $obj->$authticket = $this->AuthenticationTicket;
        
        $headers = new \SoapHeader('http://centiro.com/facade/shared/1/0/datacontract', 'Authenticate', $obj);
        return [$headers];
    }
    
    private function call($request, $arguments){
        if($arguments !== null){
            $arguments = (object)$arguments;
        }

        try{
            $result = $this->client->$request($arguments);
            //'CancelShipment' is the only request with an empty response but true/false in the header instead
            if(is_soap_fault($result) || false === $result || (NULL === $result && $request != 'CancelShipment')){
                $message = '';
                if(isset($result->faultcode)){
                    $message .= "SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})".PHP_EOL;
                }
                $lastResponse = $this->client->__getLastResponse();
                if(false !== strpos($lastResponse,'BadRequest')){
                    $lastResponse = substr($lastResponse, strpos($lastResponse,'BadRequest'));
                    $lastResponse = substr($lastResponse, 0, strpos($lastResponse,'</string>'));
                }
                $message .= PHP_EOL . PHP_EOL . '%%' . $lastResponse  . '&&' . PHP_EOL . PHP_EOL;

                throw new \Exception($message);
            }else{
                return $result;
            }
        } catch (\Exception $e){
            
            error_log(__FILE__ . ': line ' . __LINE__ . PHP_EOL . var_export($this->client->__getLastRequest(),1));
            error_log( __FILE__ . ': line ' . __LINE__ . PHP_EOL . $e->__toString() );
            throw new \Exception($e->__toString());
        }
    }
    
    public function addShipment($arguments){
        return $this->call('AddShipment', $arguments);
    }
    
    public function addMasterShipment($arguments){
        $printformat = 'Zebra';
        if(isset($arguments['printformat'])){
            $printformat = $arguments['printformat'];
            unset($arguments['printformat']);
        }
        return $this->call('AddMasterShipment', MasterShipment::_addMasterShipmentRequest(MasterShipment::_getMasterShipment($arguments), $printformat));
    }
    
    /**
     * Asendia only uses this for printing labels
     * printformat is 'Pdf' or 'Zebra' or 'Eps' or 'Tiff'
     */
    public function addAndPrintShipment($arguments){
        $printformat = 'Zebra';
        if(isset($arguments['printformat'])){
            $printformat = $arguments['printformat'];
            unset($arguments['printformat']);
        }
        return $this->call('AddAndPrintShipment', Shipment::_getAddAndPrintRequest(Shipment::_getShipment($arguments), $printformat));
    }
    
    public function getShipmentDocuments($arguments){
        return $this->call('GetShipmentDocuments', $arguments);
    }
    
    public function uploadPrintShipmentBatch($arguments){
        //not used for Asendia
    }
    
    public function closeShipments($arguments){
        return $this->call('CloseShipments', $arguments);
    }
    
    public function cancelShipment($arguments){
        return $this->call('CancelShipment', $arguments);
    }
    
    public function addAndPrintParcels($arguments){
        //not used for Asendia
    }
    
    public function printParcels($arguments){
        //not used for Asendia
    }
    
    public function deleteParcel($arguments){
        //not used for Asendia
    }
}

