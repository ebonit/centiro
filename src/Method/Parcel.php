<?php
/*
 *  Centiro shipment API Client for Asendia
 * 
 *  (c) Eric Bontenbal web development <info@ericbontenbal.nl>
 */

namespace Ebonit\Centiro\Method;

use \Ebonit\Centiro\Method\Attribute;
use \Ebonit\Centiro\Method\OrderLine;

class Parcel
{
    
    private static $Attributes = NULL;
    private static $DeliveryInstruction1 = NULL; //string 50
    private static $DeliveryInstruction2 = NULL; //string 50
    private static $DeliveryInstruction3 = NULL; //string 50
    private static $DeliveryInstruction4 = NULL; //string 50
    private static $GoodsMarking = NULL; //string 50
    private static $Height = NULL;//decimal mtr
    private static $Length = NULL;//decimal mtr
    private static $LoadingMeasure = NULL;//decimal mtr
    private static $NetWeight = NULL;//decimal kg
    private static $OrderLines = NULL;
    private static $ParcelIdentifier = null; //string 50 minimal 1 unique MANDATORY
    private static $TypeOfGoods = NULL; //string 82
    private static $TypeOfPackage = NULL; //string 50
    private static $Value = NULL;//decimal eur
    private static $Volume = NULL;//integer
    private static $Weight = NULL;//decimal Kg
    private static $Width = NULL;//decimal mtr
    
    private static $fields = ['Attributes','DeliveryInstruction1','DeliveryInstruction2','DeliveryInstruction3',
        'DeliveryInstruction4','GoodsMarking','Height','Length','LoadingMeasure','NetWeight','OrderLines',
        'ParcelIdentifier','TypeOfGoods','TypeOfPackage','Value','Volume','Weight','Width'];
    
    public static function _getParcels($arguments){
        $parcels = [];
        foreach($arguments as $parcel){
            $parcels[] = self::_getParcel($parcel);
        }
        return $parcels;
    }
    
    public static function _getParcel($arguments){
        $parcel = [];
        foreach($arguments as $k => $v){
            if($k == 'Attributes'){
                $v = Attribute::_getAttributes($v);
            }
            if($k == 'OrderLines'){
                $v = OrderLine::_getOrderLines($v);
            }
            self::$$k = $v;
        }
        
        foreach(self::$fields as $field){
            if(NULL !== self::$$field || self::$$field != 0.00){
                $parcel[$field] = self::$$field;
            }
        }
        
        return $parcel;
    }
}
    