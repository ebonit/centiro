<?php
/*
 *  Centiro shipment API Client for Asendia
 * 
 *  (c) Eric Bontenbal web development <info@ericbontenbal.nl>
 */

namespace Ebonit\Centiro\Method;

use Ebonit\Centiro\Method\Address;
use Ebonit\Centiro\Method\Attribute;
use Ebonit\Centiro\Method\Date;
use Ebonit\Centiro\Method\OrderLine;
use Ebonit\Centiro\Method\Parcel;

class Shipment
{
    
    private static $Addresses = NULL;
    private static $Attributes = NULL;    
    private static $CashOnDeliveryAmount = NULL;
    private static $CashOnDeliveryCurrency = NULL;
    private static $CashOnDeliveryReference = NULL;
    private static $CreateReturnShipment = NULL;
    private static $Currency = 'EUR';
    private static $Dates = NULL;
    private static $DeliveryInstructions1 = NULL;
    private static $DeliveryInstructions2 = NULL;
    private static $DeliveryInstructions3 = NULL;
    private static $DeliveryInstructions4 = NULL;
    private static $FreighCost = NULL;//decimal eur
    private static $FreightCurrency = NULL;
    private static $FreightPrice = NULL;//decimal eur
    private static $InsuranceAmount = NULL;
    private static $LoadingMeasure = NULL;
    private static $ModeOfTransport = 'ACSS';
    private static $NumberOfEURPallets = NULL;
    private static $OrderLines = NULL;
    private static $OrderNumber = NULL;
    private static $Parcels = NULL;    
    private static $Reference = NULL;
    private static $RouteNumberreference = NULL;
    private static $SenderCode = NULL;
    private static $SequenceNumber = NULL;
    private static $ShipDate = NULL;
    private static $ShipmentIdentifier = NULL;
    private static $ShipmentType = 'OUTB';
    private static $TermOfDelivery = NULL;
    private static $TermsOfDeliveryLocation = NULL;
    private static $TransportPayer = NULL;
    private static $TransportPayerCustNo = NULL;
    private static $TripNumber = NULL;
    private static $Value = NULL;
    private static $Volume = NULL;
    private static $Weight = NULL;
    
    private static $fields = ['Addresses','Attributes','CashOnDeliveryAmount','CashOnDeliveryCurrency','CashOnDeliveryReference','CreateReturnShipment','Currency',
                              'Dates','DeliveryInstructions1','DeliveryInstructions2','DeliveryInstructions3','DeliveryInstructions4','FreighCost','FreightCurrency',
                              'FreightPrice','InsuranceAmount','LoadingMeasure','ModeOfTransport','NumberOfEURPallets','OrderLines','OrderNumber','Parcels',
                              'Reference','RouteNumberreference','SenderCode','SequenceNumber','ShipDate','ShipmentIdentifier','ShipmentType','TermOfDelivery',
                              'TermsOfDeliveryLocation','TransportPayer','TransportPayerCustNo','TripNumber','Value','Volume','Weight'
                             ];
    
    public static function _getShipment($arguments){

        foreach($arguments as $k => $v){
            $k = ucfirst($k);
            if($k == 'Addresses'){
                $v = Address::_getAddresses($v);
            }
            if($k == 'Attributes'){
                $v = Attribute::_getAttributes($v);
            }
            if($k == 'Dates'){
                $v = Date::_getDates($v);
            }
            if($k == 'OrderLines'){
                $v = OrderLine::_getOrderLines($v);
            }
            if($k == 'Parcels'){
                $v = Parcel::_getParcels($v);
            }
            self::$$k = $v;
        }
        
        foreach(self::$fields as $field){
            if(NULL !== self::$$field){
                $shipment[$field] = self::$$field;
            }
        }

        return $shipment;
    }
    
    
    public static function _getAddAndPrintRequest($shipment, $labelType = 'Zebra'){

        if(NULL === $shipment){
            return false;
        }
        $AddAndPrintShipmentRequest['LabelType'] = $labelType;
        $AddAndPrintShipmentRequest['Shipment'] = $shipment;
   
        return $AddAndPrintShipmentRequest;
    }
}

        