<?php
/*
 *  Centiro shipment API Client for Asendia
 * 
 *  (c) Eric Bontenbal web development <info@ericbontenbal.nl>
 */

namespace Ebonit\Centiro\Method;

class Date
{
    
    private static $Code = null;
    private static $Date = null; //DateTime 0000-00-00T00:00:00
    
    public static function _getDates($arguments){
        $dates = [];
        foreach($arguments as $date){
            $dates[] = self::_getDate($date);
        }
        return $dates;
    }
    
    public static function _getDate($arguments){
        $date = [];
        foreach($arguments as $k => $v){
            self::$$k = $v;
        }
        $date['Code'] = self::$Code;
        $date['Date'] = self::$Date;
        
        return $date;
    }
}