<?php
/*
 *  Centiro shipment API Client for Asendia
 * 
 *  (c) Eric Bontenbal web development <info@ericbontenbal.nl>
 */

namespace Ebonit\Centiro\Method;

class Attribute
{
    private static $Code = null;
    private static $Value = null;    
    
    public static function _getAttributes($arguments){
        $attributes = [];
        foreach($arguments as $k=>$v){
            $attributes[] = self::_getAttribute([$k => $v]);
        }
        return $attributes;
    }
    
    public static function _getAttribute($arguments){
        $attribute = [];
        foreach($arguments as $k => $v){
            self::$Code = $k;
            self::$Value = $v;
        }
        $attribute['Code'] = self::$Code;
        $attribute['Value'] = self::$Value;
        
        return $attribute;
    }
}