<?php
/*
 *  Centiro shipment API Client for Asendia
 * 
 *  (c) Eric Bontenbal web development <info@ericbontenbal.nl>
 */

namespace Ebonit\Centiro\Method;

class Address
{
    private static $Address1 = NULL;//mandatory
    private static $Address2 = NULL;
    private static $Address3 = NULL;
    private static $AddressType = 'Receiver';//mandatory
    private static $CellPhone = NULL;//conditional (sms)
    private static $City = NULL;//mandatory
    private static $Contact = NULL;
    private static $CustomerNumber = NULL;
    private static $Email = NULL;
    private static $Fax = NULL;
    private static $FreeText1 = NULL;
    private static $FreeText2 = NULL;
    private static $FreeText3 = NULL;
    private static $FreeText4 = NULL;
    private static $ISOCountry = NULL; //iso2 - mandatory
    private static $Name = NULL;//mandatory
    private static $Phone = NULL;//conditional (sms)
    private static $State = NULL;
    private static $VatNo = NULL;
    private static $ZipCode = NULL;//mandatory
    
    private static $fields = [
        'Address1','Address2','Address3','AddressType','CellPhone','City',
        'Contact','CustomerNumber','Email','Fax','FreeText1','FreeText2',
        'FreeText3','FreeText4','ISOCountry','Name','Phone','State','VatNo','ZipCode'
    ];
    
    public static function _getAddresses($arguments){
        $addresses = [];
        foreach($arguments as $index =>$address){
            $addresses[] = self::_getAddress($address);
        }
        return $addresses;
    }
    
    private static function _getAddress($arguments){
        $address = [];
        foreach($arguments as $k => $v){
            self::$$k = $v;
        }
        foreach(self::$fields as $field){
            if(NULL !== self::$$field){
                $address[$field] = self::$$field;
            }
        }
        return $address;
    }
}