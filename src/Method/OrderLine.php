<?php
/*
 *  Centiro shipment API Client for Asendia
 * 
 *  (c) Eric Bontenbal web development <info@ericbontenbal.nl>
 */

namespace Ebonit\Centiro\Method;

class OrderLine
{
    
    private static $Attributes = NULL;
    private static $CountryOfOrigin = NULL;
    private static $Currency = NULL;
    private static $Description1 = NULL;
    private static $Description2 = NULL;
    private static $FreeFloat1 = NULL;
    private static $FreeFloat2 = NULL;
    private static $HarmonizationCode = NULL;
    private static $OrderLineNumber = NULL;//integer
    private static $OrderNumber = NULL;
    private static $ProductNumber = NULL;
    private static $PurchaseOrderLineNumber = NULL; //integer
    private static $PurchaseOrderNumber = NULL;
    private static $QuantityOrdered = NULL;
    private static $QuantityShipped = NULL;
    private static $UnitOfMeasure = NULL;
    private static $UnitPrice = NULL;
    private static $UnitWeight = NULL; //weight per piece in Kg
    private static $Volume = NULL; //weight in Kg
    private static $Weight = NULL;
    
    private static $fields = ['Attributes','CountryOfOrigin','Currency','Description1','Description2',
        'FreeFloat1','FreeFloat2','HarmonizationCode','OrderLineNumber','OrderNumber',
        'ProductNumber','PurchaseOrderLineNumber','PurchaseOrderNumber','QuantityOrdered',
        'QuantityShipped','UnitOfMeasure','UnitPrice','UnitWeight','Volume','Weight'];
    
    public static function _getOrderLines($arguments){

        $orderLines = [];
        foreach($arguments as $orderLine){
            $orderLines[] = self::_getOrderLine($orderLine);
        }
        return $orderLines;
    }
    
    public static function _getOrderLine($arguments){
        $orderLine = [];
        foreach($arguments as $k => $v){
            self::$$k = $v;
        }
        
        foreach(self::$fields as $field){
            if(NULL !== self::$$field){
                $orderLine[$field] = self::$$field;
            }
        }
        
        return $orderLine;
    }
}
        