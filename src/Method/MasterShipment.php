<?php
/*
 *  Centiro shipment API Client for Asendia
 * 
 *  (c) Eric Bontenbal web development <info@ericbontenbal.nl>
 */

namespace Ebonit\Centiro\Method;

use Ebonit\Centiro\Method\Address;
use Ebonit\Centiro\Method\ChildShipmentIdentifiers;
use Ebonit\Centiro\Method\Date;

class MasterShipment
{
    private static $Addresses = NULL;
    private static $Attributes = NULL;
    private static $ChildShipmentIdentifiers = NULL;
    private static $DeliveryInstruction = NULL;
    private static $LoadingMeasure = NULL;
    private static $ModeOfTransport = 'MASTERSHP';
    private static $OrderNumber = NULL;
    private static $SenderCode = NULL;
    private static $SequenceNumber = NULL;
    private static $ShipDate = NULL;
    private static $ShipmentIdentifier = NULL;
    private static $Value = NULL;
    private static $Volume = NULL;
    private static $Weight = NULL;
    
    private static $fields = ['Addresses', 'Attributes', 'ChildShipmentIdentifiers', 'DeliveryInstruction', 'LoadingMeasure', 
                              'ModeOfTransport', 'OrderNumber', 'SenderCode', 'SequenceNumber', 'ShipDate', 'ShipmentIdentifier', 
                              'Value', 'Volume', 'Weight'];
    
    public static function _getMasterShipment($arguments){
        foreach($arguments as $k => $v){
            $k = ucfirst($k);
            if($k == 'Addresses'){
                $v = Address::_getAddresses($v);
            }
            if($k == 'Attributes'){
                $v = Attribute::_getAttributes($v);
            }
            self::$$k = $v;
        }
        
        foreach(self::$fields as $field){
            if(NULL !== self::$$field){
                $masterShipment[$field] = self::$$field;
            }
        }

        return $masterShipment;
    }
    
    public static function _addMasterShipmentRequest($masterShipment, $labelType = 'Zebra'){

        if(NULL === $masterShipment){
            return false;
        }
        $AddMasterShipmentRequest['LabelType'] = $labelType;
        $AddMasterShipmentRequest['MasterShipment'] = $masterShipment;

        return $AddMasterShipmentRequest;
    }
}

