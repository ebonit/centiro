<?php
/*
 *  Centiro shipment API Client for Asendia
 * 
 *  (c) Eric Bontenbal web development <info@ericbontenbal.nl>
 */

namespace Ebonit\Centiro;

class Client
{
    
    private $client;
    
    public function __construct($user, $password, $test = false){
        
        $this->client = new CentiroWsdlClient($user, $password, $test);
    }
    
    public function __call($name, $arguments){
        if(!method_exists($this->client, $name)){
            throw new \Exception("The called method '{$name}' does not excist!");
        }
        try{ 
            $result = $this->client->$name($arguments[0]);
            return $result;
        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
        
    }
}