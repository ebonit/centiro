<?php
/*
 *  Centiro shipment API Client for Asendia
 * 
 *  (c) Eric Bontenbal web development <info@ericbontenbal.nl>
 * 
 * this depends on stiwl/php-print-ipp
 * composer require stiwl/php-print-ipp
 * 
 */

namespace Ebonit\Centiro;

use Ebonit\Centiro\Lib\PhpPrintIpp\CupsPrintIPP;
use Ebonit\Centiro\Lib\PhpPrintIpp\PrintIPP;

class Printer
{
    private $host;
    private $port;
    private $queue;
    private $printer;
    
    private $logactive = false;
    private $logdestination;
    private $logtype; //fi 'file'
    private $loglevel; //1-3
    
    /**
     * 
     * @param String $printer
     */
    public function __construct($host, $port, $queue, $printer = 'cups'){//$printer = 'cups'){
        $this->printer = $printer;
        $this->host = $host;
        $this->port = $port;
        $this->queue = $queue;
    }
    
    public function setLog($active, $destination, $destinationType, $level){
        $this->logactive = $active;
        $this->logdestination = $destination;
        $this->logtype = $destinationType;
        $this->loglevel = $level;
    }
    
    public function test($format = 'A6'){
        if(!file_exists(__DIR__."/Printertest/test{$format}.pdf")){
            throw new Exception('At this moment only A6 format is available'.PHP_EOL.'You can add PDF tests in  '.__DIR__."/Printertest/");
            return false;
        }
        
        if($this->printDocument(__DIR__."/Printertest/test{$format}.pdf")){
            return true;
        }
        return false;
    }
    
    /**
     * 
     * @param string $document, /path/to/the/document
     * @return Boolean
     * 
     */
    public function printDocument($document, $copies = 1){
        
        if($this->queue === NULL){
            throw new Exception("For cups printers the Queue is mandatory");
            return false;
        }
        if($this->printer == 'cups'){
            $printer = new CupsPrintIPP();
            $printer->setCharset('utf-8');
            $printeruri = "ipp://{$this->host}:{$this->port}/{$this->queue}";
        }else{
            $printer = new PrintIPP();
            $printer->setCharset('utf-8');
            $printeruri = "ipp://{$this->host}:{$this->port}/{$this->queue}";
        }
        if($this->logactive){
            $printer->setLog($this->logdestination, $this->logtype, $this->loglevel);
        }
//$printer->debug_level = 0;
        $printer->setHost($this->host);
        $printer->setPrinterURI($printeruri);
        $printer->unsetFormFeed();
        $printer->setData($document);
        $printer->setCopies($copies);
//die(var_dump($printer));

        $result = $printer->printJob();
        return $result;
    }
}
