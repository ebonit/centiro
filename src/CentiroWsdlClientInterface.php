<?php
/*
 *  Centiro shipment API Client for Asendia
 * 
 *  (c) Eric Bontenbal web development <info@ericbontenbal.nl>
 */

namespace Ebonit\Centiro;

interface CentiroWsdlClientInterface
{
    public function addShipment($arguments);
    
    public function addMasterShipment($arguments);
    
    public function addAndPrintShipment($arguments);
    
    public function getShipmentDocuments($arguments);
    
    public function uploadPrintShipmentBatch($arguments);
    
    public function closeShipments($arguments);
    
    public function cancelShipment($arguments);
    
    public function addAndPrintParcels($arguments);
    
    public function printParcels($arguments);
    
    public function deleteParcel($arguments);    
    
}
