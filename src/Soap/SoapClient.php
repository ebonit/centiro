<?php
/*
 *  Centiro shipment API Client for Asendia
 * 
 *  (c) Eric Bontenbal web development <info@ericbontenbal.nl>
 */

namespace  Ebonit\Centiro\Soap;

class SoapClient extends \SoapClient
{
       
    function __doRequest($request, $location, $action, $version, $one_way = NULL) {
        if(strpos($request, '<ns2:Authenticate>')){
            $request = str_replace('xmlns:ns2', 'xmlns:dat', $request);
            $request = str_replace('<ns2:Authenticate>', '', $request);
            $request = str_replace('</ns2:Authenticate>', '', $request);
        }elseif(strpos($request, '<ns3:Authenticate>')){
            $request = str_replace('xmlns:ns3', 'xmlns:dat', $request);
            $request = str_replace('<ns3:Authenticate>', '', $request);
            $request = str_replace('</ns3:Authenticate>', '', $request);
        }elseif(strpos($request, '<ns4:Authenticate>')){
            $request = str_replace('xmlns:ns4', 'xmlns:dat', $request);
            $request = str_replace('<ns4:Authenticate>', '', $request);
            $request = str_replace('</ns4:Authenticate>', '', $request);
        }
        $this->__last_request = $request;
        
        return parent::__doRequest($request, $location, $action, $version, $one_way);
    } 
}
